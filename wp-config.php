<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'car' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '%X6O9N)ycuB8el^=44~np{Fi;Fum{>81,.NH$LUznr8GU}icIs5sv{9%x4?$uKeB' );
define( 'SECURE_AUTH_KEY',  '+QgQv67~9PwLge0oGcsuJc`+,S.~@-E4]DB(=ps(^D%:UCK4&)5FoL#DzOxYnNM~' );
define( 'LOGGED_IN_KEY',    'uq~as#aEw4)NUs$3sUA7lk!2Bkr&8Qc8yE&gz fV&Um1${b{p6V:+!#(.5V5~Q@a' );
define( 'NONCE_KEY',        '/ahsGk;c!Ic:)qmTRsKb@xyIh.oa]PQ,jXj1tH_wv|HAHQq>uif~[4&RU-6Ex1FJ' );
define( 'AUTH_SALT',        'l)&n[g`15*)O6YMg6b2UA>Q5 )Y>1=VT5D%3IAQP<We_LRD~$HM[`Z0Ntg`/l16$' );
define( 'SECURE_AUTH_SALT', '5Oub=co{su7g=!)~5|}Ev_%:#Z/6G=WBJ+m`Kt;vrf_!|M Xw?Q}7$&{VJWZ#+Im' );
define( 'LOGGED_IN_SALT',   'K[^wh~U1fFO>vV/c$]F_df~_iOXj6nV.+rcyMiS!g2a&)@qall3NN.#tjBfW_v.I' );
define( 'NONCE_SALT',       'SAvNc38S?)XYMxY=1M:u$oQ(%.]rDUWM>f.Flhqaj#mc(({%o*(bE>%QM1)G-Sfx' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
