<?php
/**
 * cars functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package cars
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

add_image_size( 'small', 110, 75 ); // 220 pixels wide by 180 pixels tall, soft proportional crop mode

if ( ! function_exists( 'cars_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function cars_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on cars, use a find and replace
		 * to change 'cars' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'cars', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'main_menu' => esc_html__( 'Main Menu', 'cars' ),
				'footer_menu' => esc_html__( 'Footer Menu', 'cars' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'cars_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'cars_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function cars_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'cars_content_width', 640 );
}
add_action( 'after_setup_theme', 'cars_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cars_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'cars' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'cars' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
    register_sidebar(array(
        'name' =>esc_html__('Footer Widget First','Car'),
        'description'=>esc_html__('This is Left Footer Widget','Car'),
        'id'=>'footer_widget_one',
        'before_widget'=>'<div class="pacz-col-1-4"><section id="text-7" class="widget widget_text">',
        'after_widget'=>'</section></div>',
        'before_title'=>'<div class="widgettitle">',
        'after_title'=>'</div>'
    ));

    register_sidebar(array(
        'name' =>esc_html__('Footer Widget Second','Car'),
        'description'=>esc_html__('This is Middle Footer Widget','Car'),
        'id'=>'footer_widget_two',
        'before_widget'=>'<div class="pacz-col-1-4"><section id="text-7" class="widget widget_text">',
        'after_widget'=>'</section></div>',
        'before_title'=>'<div class="widgettitle">',
        'after_title'=>'</div>'
    ));
    register_sidebar(array(
        'name' =>esc_html__('Footer Widget Third','Car'),
        'description'=>esc_html__('This is Middle Footer Widget','Car'),
        'id'=>'footer_widget_three',
        'before_widget'=>'<div class="pacz-col-1-4"><section id="text-7" class="widget widget_text">',
        'after_widget'=>'</section></div>',
        'before_title'=>'<div class="widgettitle">',
        'after_title'=>'</div>'
    ));
    register_sidebar(array(
        'name' =>esc_html__('Footer Widget Fourth','Car'),
        'description'=>esc_html__('This is Right Footer Widget','Car'),
        'id'=>'footer_widget_four',
        'before_widget'=>'<div class="pacz-col-1-4"><section id="text-7" class="widget widget_text">',
        'after_widget'=>'</section></div>',
        'before_title'=>'<div class="widgettitle">',
        'after_title'=>'</div>'
    ));
}
add_action( 'widgets_init', 'cars_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function cars_scripts() {
	wp_enqueue_style( 'cars-style', get_stylesheet_uri(), array(), _S_VERSION );

    //for style
    wp_enqueue_style('bootstrap',get_theme_file_uri('styles/css/bootstrap.min.css'),array(),'');
    wp_enqueue_style('slick',get_theme_file_uri('styles/css/slick/slick.css'),array(),'');
    wp_enqueue_style('font','https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;1,300&display=swap',array(),'');
    wp_enqueue_style('select2','https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css',array(),'');
    wp_enqueue_style('fontawesome','https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',array(),'');
    wp_enqueue_style('jqueryui','https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.min.css',array(),'');
    wp_enqueue_style('slick-theme',get_theme_file_uri('styles/css/slick/slick-theme.css'),array(),'');
    wp_enqueue_style('style1',get_theme_file_uri('styles/css/styles.css'),array(),'');
    wp_enqueue_style('pacz',get_theme_file_uri('css/pacz-styles.css'),array(),'');
    wp_enqueue_style('pacz-blog',get_theme_file_uri('styles/css/pacz-blog.css'),array(),'');

    wp_enqueue_style('common',get_theme_file_uri('styles/css/shortcode/common-shortcode.css'),array(),'');
    wp_enqueue_style('fonticon',get_theme_file_uri('styles/css/fonticon-custom.min.css'),array(),'');
    wp_enqueue_style('motoro',get_theme_file_uri('css/motoro-dynamic.css'),array(),'');
    wp_enqueue_style('frontend',get_theme_file_uri('css/frontend-custom.css'),array(),'');
    wp_enqueue_style('swiper',get_theme_file_uri('css/swiper.min.css'),array(),'');

    wp_enqueue_style('submitlisting',get_theme_file_uri('css/submitlisting-custom.css'),array(),'');
    wp_enqueue_style('woocommercedssd',get_theme_file_uri('styles/css/pacz-woocommerce.css'),array(),'');
    wp_enqueue_style('custom',get_theme_file_uri('css/custom.css'),array(),'');


    //for script
    wp_enqueue_script('bootstrapjs',get_theme_file_uri('js/bootstrap.min.js'),array('jquery'),'',true);
    wp_enqueue_script('swiperjs',get_theme_file_uri('js/swiper.min.js'),array(),'',true);
    wp_enqueue_script('select2js','https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js',array(),'',true);
    wp_enqueue_script('customjs',get_theme_file_uri('js/custom.js'),array(),'',true);


}
add_action( 'wp_enqueue_scripts', 'cars_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}


//taxonomy

/**
 * Register a 'genre' taxonomy for post type 'book'.
 *
 * @see register_post_type for registering post types.
 */
function wpdocs_create_book_tax() {
    register_taxonomy( 'country', 'product', array(
        'label'        => __( 'Country', 'textdomain' ),
        'rewrite'      => array( 'slug' => 'country' ),
        'hierarchical' => true,
    ) );
}
add_action( 'init', 'wpdocs_create_book_tax', 0 );






//NEW FIELD CREATE


/*
* Adding extra field on New product popup/without popup form
*/
add_action( 'dokan_new_product_after_product_tags','new_product_field',10 );

function new_product_field(){ ?>

    <div class="dokan-form-group">

        <input type="text" class="dokan-form-control" name="total_seat" placeholder="<?php esc_attr_e( 'Total Seat', 'dokan-lite' ); ?>">
    </div>

    <?php
}

/*
* Saving product field data for edit and update
*/

add_action( 'dokan_new_product_added','save_add_product_meta', 10, 2 );
add_action( 'dokan_product_updated', 'save_add_product_meta', 10, 2 );

function save_add_product_meta($product_id, $postdata){

    if ( ! dokan_is_user_seller( get_current_user_id() ) ) {
        return;
    }

    if ( ! empty( $postdata['total_seat'] ) ) {
        update_post_meta( $product_id, 'total_seat', $postdata['total_seat'] );
    }
}

/*
* Showing field data on product edit page
*/

add_action('dokan_product_edit_after_product_tags','show_on_edit_page',99,2);

function show_on_edit_page($post, $post_id){
    $total_seat         = get_post_meta( $post_id, 'total_seat', true );
    ?>
    <div class="dokan-form-group">
        <input type="hidden" name="total_seat" id="dokan-edit-product-id" value="<?php echo esc_attr( $post_id ); ?>"/>
        <label for="total_seat" class="form-label"><?php esc_html_e( 'Total Seat', 'dokan-lite' ); ?></label>
        <?php dokan_post_input_box( $post_id, 'total_seat', array( 'placeholder' => __( 'Total Seat', 'dokan-lite' ), 'value' => $total_seat ) ); ?>
        <div class="dokan-product-title-alert dokan-hide">
            <?php esc_html_e( 'Please enter Total Seat  !', 'dokan-lite' ); ?>
        </div>
    </div> <?php

}

// showing on single product page
add_action('woocommerce_single_product_summary','show_product_code',13);

function show_product_code(){
    global $product;

    if ( empty( $product ) ) {
        return;
    }
    $total_seat = get_post_meta( $product->get_id(), 'total_seat', true );

    if ( ! empty( $total_seat ) ) {
        ?>
        <span class="details"><?php echo esc_attr__( 'Total Seat:', 'dokan-lite' ); ?> <strong><?php echo esc_attr( $total_seat ); ?></strong></span>
        <?php
    }
}


//NEW FIELD CREATE


/*
* Adding extra field on New product popup/without popup form
*/
add_action( 'dokan_new_product_after_product_tags','new_product_field1',10 );

function new_product_field1(){ ?>

    <div class="dokan-form-group">
      <select name="car_type" class="dokan-form-control">
          <option value=""></option>
      </select>
        <input type="text" class="dokan-form-control" name="car_type" placeholder="<?php esc_attr_e( 'Car ssType', 'dokan-lite' ); ?>">
    </div>

    <?php
}

/*
* Saving product field data for edit and update
*/

add_action( 'dokan_new_product_added','save_add_product_meta1', 10, 2 );
add_action( 'dokan_product_updated', 'save_add_product_meta1', 10, 2 );

function save_add_product_meta1($product_id, $postdata){

    if ( ! dokan_is_user_seller( get_current_user_id() ) ) {
        return;
    }

    if ( ! empty( $postdata['car_type'] ) ) {
        update_post_meta( $product_id, 'car_type', $postdata['car_type'] );
    }
}

/*
* Showing field data on product edit page
*/

add_action('dokan_product_edit_after_product_tags','show_on_edit_page1',99,2);

function show_on_edit_page1($post, $post_id){
    $car_type         = get_post_meta( $post_id, 'car_type', true );
    ?>
    <div class="dokan-form-group" id="dokan-edit-product-id">

        <input type="hidden" name="car_type" id="dokan-edit-product-id" value="<?php echo esc_attr( $post_id ); ?>"/>
        <label for="car_type" class="form-label"><?php esc_html_e( 'Car Type', 'dokan-lite' ); ?></label>
        <?php
        dokan_post_input_box($post_id, 'car_type', array('options' => array('Automatic' => __('Automatic', 'dokan-lite'), 'Manual' => __('Manual', 'dokan-lite'))), 'select');

         ?>

    </div> <?php

}

// showing on single product page
add_action('woocommerce_single_product_summary','show_product_code1',13);

function show_product_code1(){
    global $product;

    if ( empty( $product ) ) {
        return;
    }
    $car_type = get_post_meta( $product->get_id(), 'car_type', true );

    if ( ! empty( $car_type ) ) {
        ?>
        <span class="details"><?php echo esc_attr__( 'Car Type:', 'dokan-lite' ); ?> <strong><?php echo esc_attr( $car_type ); ?></strong></span>
        <?php
    }
}




//NEW FIELD CREATE


/*
* Adding extra field on New product popup/without popup form
*/
add_action( 'dokan_new_product_after_product_tags','new_product_field3',10 );

function new_product_field3(){ ?>

    <div class="dokan-form-group">

        <input type="text" class="dokan-form-control" name="engine_type" placeholder="<?php esc_attr_e( 'engine_type', 'dokan-lite' ); ?>">
    </div>

    <?php
}

/*
* Saving product field data for edit and update
*/

add_action( 'dokan_new_product_added','save_add_product_meta3', 10, 2 );
add_action( 'dokan_product_updated', 'save_add_product_meta3', 10, 2 );

function save_add_product_meta3($product_id, $postdata){

    if ( ! dokan_is_user_seller( get_current_user_id() ) ) {
        return;
    }

    if ( ! empty( $postdata['engine_type'] ) ) {
        update_post_meta( $product_id, 'engine_type', $postdata['engine_type'] );
    }
}

/*
* Showing field data on product edit page
*/

add_action('dokan_product_edit_after_product_tags','show_on_edit_page3',99,2);

function show_on_edit_page3($post, $post_id){
    $engine_type         = get_post_meta( $post_id, 'engine_type', true );
    ?>
    <div class="dokan-form-group">
        <input type="hidden" name="total_seat" id="dokan-edit-product-id" value="<?php echo esc_attr( $post_id ); ?>"/>
        <label for="total_seat" class="form-label"><?php esc_html_e( 'Engine type', 'dokan-lite' ); ?></label>
        <?php dokan_post_input_box( $post_id, 'engine_type', array( 'placeholder' => __( 'Engine Type', 'dokan-lite' ), 'value' => $engine_type ) ); ?>
        <div class="dokan-product-title-alert dokan-hide">
            <?php esc_html_e( 'Please enter engine_type  !', 'dokan-lite' ); ?>
        </div>
    </div> <?php

}

// showing on single product page
add_action('woocommerce_single_product_summary','show_product_code3',13);

function show_product_code3(){
    global $product;

    if ( empty( $product ) ) {
        return;
    }
    $engine_type= get_post_meta( $product->get_id(), 'engine_type', true );

    if ( ! empty( $total_seat ) ) {
        ?>
        <span class="details"><?php echo esc_attr__( 'Engine Type:', 'dokan-lite' ); ?> <strong><?php echo esc_attr( $engine_type); ?></strong></span>
        <?php
    }
}



//NEW FIELD CREATE


/*
* Adding extra field on New product popup/without popup form
*/
add_action( 'dokan_new_product_after_product_tags','new_product_field4',10 );

function new_product_field4(){ ?>

    <div class="dokan-form-group">

        <input type="text" class="dokan-form-control" name="warranty" placeholder="<?php esc_attr_e( 'warranty', 'dokan-lite' ); ?>">
    </div>

    <?php
}

/*
* Saving product field data for edit and update
*/

add_action( 'dokan_new_product_added','save_add_product_meta4', 10, 2 );
add_action( 'dokan_product_updated', 'save_add_product_meta4', 10, 2 );

function save_add_product_meta4($product_id, $postdata){

    if ( ! dokan_is_user_seller( get_current_user_id() ) ) {
        return;
    }

    if ( ! empty( $postdata['warranty'] ) ) {
        update_post_meta( $product_id, 'warranty', $postdata['warranty'] );
    }
}

/*
* Showing field data on product edit page
*/

add_action('dokan_product_edit_after_product_tags','show_on_edit_page4',99,2);

function show_on_edit_page4($post, $post_id){
    $warranty        = get_post_meta( $post_id, 'warranty', true );
    ?>
    <div class="dokan-form-group">
        <input type="hidden" name="warranty" id="dokan-edit-product-id" value="<?php echo esc_attr( $post_id ); ?>"/>
        <label for="warranty" class="form-label"><?php esc_html_e( 'warranty', 'dokan-lite' ); ?></label>
        <?php dokan_post_input_box( $post_id, 'warranty', array( 'placeholder' => __( 'warranty', 'dokan-lite' ), 'value' => $warranty ) ); ?>
        <div class="dokan-product-title-alert dokan-hide">
            <?php esc_html_e( 'Please enter warranty  !', 'dokan-lite' ); ?>
        </div>
    </div> <?php

}

// showing on single product page
add_action('woocommerce_single_product_summary','show_product_code4',13);

function show_product_code4(){
    global $product;

    if ( empty( $product ) ) {
        return;
    }
    $warranty= get_post_meta( $product->get_id(), 'warranty', true );

    if ( ! empty( $warranty ) ) {
        ?>
        <span class="details"><?php echo esc_attr__( 'warranty:', 'dokan-lite' ); ?> <strong><?php echo esc_attr( $warranty); ?></strong></span>
        <?php
    }
}




//NEW FIELD CREATE


/*
* Adding extra field on New product popup/without popup form
*/
add_action( 'dokan_new_product_after_product_tags','new_product_field5',10 );

function new_product_field5(){ ?>

    <div class="dokan-form-group">

        <input type="text" class="dokan-form-control" name="number_of_doors" placeholder="<?php esc_attr_e( 'number_of_doors', 'dokan-lite' ); ?>">
    </div>

    <?php
}

/*
* Saving product field data for edit and update
*/

add_action( 'dokan_new_product_added','save_add_product_meta5', 10, 2 );
add_action( 'dokan_product_updated', 'save_add_product_meta5', 10, 2 );

function save_add_product_meta5($product_id, $postdata){

    if ( ! dokan_is_user_seller( get_current_user_id() ) ) {
        return;
    }

    if ( ! empty( $postdata['number_of_doors'] ) ) {
        update_post_meta( $product_id, 'number_of_doors', $postdata['number_of_doors'] );
    }
}

/*
* Showing field data on product edit page
*/

add_action('dokan_product_edit_after_product_tags','show_on_edit_page5',99,2);

function show_on_edit_page5($post, $post_id){
    $number_of_doors        = get_post_meta( $post_id, 'number_of_doors', true );
    ?>
    <div class="dokan-form-group">
        <input type="hidden" name="number_of_doors" id="dokan-edit-product-id" value="<?php echo esc_attr( $post_id ); ?>"/>
        <label for="number_of_doors" class="form-label"><?php esc_html_e( 'number_of_doors', 'dokan-lite' ); ?></label>
        <?php dokan_post_input_box( $post_id, 'number_of_doors', array( 'placeholder' => __( 'number_of_doors', 'dokan-lite' ), 'value' => $number_of_doors ) ); ?>
        <div class="dokan-product-title-alert dokan-hide">
            <?php esc_html_e( 'Please enter number_of_doors  !', 'dokan-lite' ); ?>
        </div>
    </div> <?php

}

// showing on single product page
add_action('woocommerce_single_product_summary','show_product_code5',13);

function show_product_code5(){
    global $product;

    if ( empty( $product ) ) {
        return;
    }
    $number_of_doors= get_post_meta( $product->get_id(), 'number_of_doors', true );

    if ( ! empty( $number_of_doors ) ) {
        ?>
        <span class="details"><?php echo esc_attr__( 'number_of_doors:', 'dokan-lite' ); ?> <strong><?php echo esc_attr( $number_of_doors); ?></strong></span>
        <?php
    }
}
