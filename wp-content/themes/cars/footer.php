<section id="pacz-footer">
    <div class="main-footer-top-padding">
    </div>
    <div class="footer-wrapper pacz-grid">
        <div class="pacz-padding-wrapper clearfix">
            <?php dynamic_sidebar('footer_widget_one'); ?>
            <?php dynamic_sidebar('footer_widget_two'); ?>
            <?php dynamic_sidebar('footer_widget_three'); ?>
            <div class="pacz-col-1-4">
                <section id="text-4" class="widget widget_text">
                    <div class="widgettitle">
                        Subscribe
                    </div>
                    <div class="textwidget">
                        <p>
                            Subscribe to our newsletter.
                        </p>
                        <input type="text" class="form-control" placeholder="Email Address">
                        <button type="submit" class="btn btn-search">Submit</button>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="clearboth">
    </div>
    <div id="sub-footer">
        <div class="pacz-grid">
            <div class="item-holder clearfix">
								<span class="pacz-footer-copyright">
									All Copyrights reserved @ 2019 - Design by Wpsixer
								</span>
                <div>
                    <div class="pacz-subfooter-logos">
                        <a href="#"><img
                                    src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/2019/03/Logo1.png"
                                    alt="logo"/></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearboth">
        </div>
    </div>
</section>
</div>
<!-- End boxed layout -->
</div>
<!-- End Theme main Wrapper -->
<div class="goto-top-btn">
    <a href="#" class="pacz-go-top"><i class="pacz-icon-angle-up"></i></a>
</div>

<?php wp_footer(); ?>
</body>

</html>