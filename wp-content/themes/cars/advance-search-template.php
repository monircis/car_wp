<?php
/**
*Template Name: Advance Search
*/
get_header();
?>
<main id="site-contents" role="main">
    <div class="container">
        <div class="row">
            <div class="column column-25">
                <div class="search-content-left">
                        <form action="" class="sidebar-search" method="GET">
                            <!-- <div class="sidebar-btn">
                                <div class="bt-item">
                                    <input type="radio" name="s-type" id="st-buy" checked>
                                    <label for="st-buy">Buy</label>
                                </div>
                                <div class="bt-item">
                                    <input type="radio" name="s-type" id="st-rent">
                                    <label for="st-rent">Rent</label>
                                </div>
                            </div> -->
                            <div>
                                <label for="location">Property Location</label>
                                <input type="text" placeholder="Write property location" id="location" name="location">
                            </div>
                            <div>
                                <label for="bedroom">No of Bedroom</label>
                                <select id="bedroom" name="bedroom">
                                    <option value="">No of Bedroom</option>
                                    <option value="1">01</option>
                                    <option value="2">02</option>
                                    <option value="3">03</option>
                                    <option value="4">04</option>
                                    <option value="5">05</option>
                                    <option value="6">06</option>
                                </select>
                            </div>
                            <div>
                                <label for="bathrooms">No of Bathrooms</label>
                                <select id="bathrooms" name="bathrooms">
                                    <option value="">No of Bathrooms</option>
                                    <option value="1">01</option>
                                    <option value="2">02</option>
                                    <option value="3">03</option>
                                    <option value="4">04</option>
                                    <option value="5">05</option>
                                    <option value="6">06</option>
                                </select>
                            </div>
                            <div>
                                <label for="garage">No of Garage</label>
                                <select id="garage" name="garage">
                                    <option value="">No of Garage</option>
                                    <option value="1">01</option>
                                    <option value="2">02</option>
                                    <option value="3">03</option>
                                    <option value="4">04</option>
                                    <option value="5">05</option>
                                    <option value="6">06</option>
                                </select>
                            </div>
                            <div class="room-size-range">
                                <div class="price-text">
                                    <label for="min_size">Size:</label>
                                    <input type="text" id="min_size" name="min_size" placeholder="Min">
                                    <input type="text" id="max_size" name="max_size" placeholder="Max">
                                </div>
                            </div>
                            <div class="price-range-wrap">
                                <div class="price-text">
                                    <label for="min_price">Price:</label>
                                    <input type="text" id="min_price" name="min_price" placeholder="Min">
                                    <input type="text" id="max_price" name="max_price" placeholder="Max">
                                </div>
                            </div>
                            <button type="submit" class="search-btn">Search Property</button>
                        </form>
                    </div>
            </div>
            <div class="column column-75">
                <div class="search-content-right">
                    <?php

                    //dynamic query

                    //location
                    if( isset($_GET['location']) && !empty($_GET['location']) ){
                        $location = array(
                            'key'     => 'location',
                            'value'   => $_GET['location'],
                            'compare' => 'LIKE',
                        );
                    }else{
                        $location = array();
                    }

                    //bedroom
                    if( isset($_GET['bedroom']) && !empty($_GET['bedroom']) ){
                        $bedroom = array(
                            'key'     => 'bed_room',
                            'value'   => $_GET['bedroom'],
                            'compare' => '=',
                        );
                    }else{
                        $bedroom = array();
                    }


                    //bath room
                    if( isset($_GET['bathrooms']) && !empty($_GET['bathrooms']) ){
                        $bathrooms = array(
                            'key'     => 'baths_bed',
                            'value'   => $_GET['bathrooms'],
                            'compare' => '=',
                        );
                    }else{
                        $bathrooms = array();
                    }

                    //garage room
                    if( isset($_GET['garage']) && !empty($_GET['garage']) ){
                        $garage = array(
                            'key'     => 'garage',
                            'value'   => $_GET['garage'],
                            'compare' => '=',
                        );
                    }else{
                        $garage = array();
                    }
                    

                    //WP_Query 
                    $args = array(
                        'post_type' => 'property',
                        'meta_query' => array(
                            'relation' => 'OR',
                            $location,
                            $bedroom,
                            $bathrooms,
                            $garage,
                            // array(
                            //     'key'     => 'price',
                            //     'value'   => array( 100, 1200 ),
                            //     'type'    => 'numeric',
                            //     'compare' => 'BETWEEN',
                            // ),
                            // array(
                            //     'key'     => 'size',
                            //     'value'   => array( 800, 1500 ),
                            //     'type'    => 'numeric',
                            //     'compare' => 'BETWEEN',
                            // ),
                        ),
                    );
                    $query = new WP_Query( $args );

                    if($query->have_posts()) :
                        while($query->have_posts()) : $query->the_post(); ?>
                        <div class="property-list">
                            <div class="pro-image">
                                <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                            </div>
                            <div class="pro-right">
                                <h3><?php the_title(); ?></h3>
                                <div><b>Price: </b>$<?php the_field('price'); ?></div>
                                <div><b>Size: </b><?php the_field('size'); ?> Sqft</div>
                                <div><b>Location: </b><?php the_field('location'); ?></div>
                                <?php the_content(); ?>
                                <div class="meta-info">
                                    <span><b>Bedroom:</b> <?php the_field('bed_room'); ?></span>
                                    <span><b>Bathroom:</b> <?php the_field('baths_bed'); ?></span>
                                    <span><b>Garage:</b> <?php the_field('garage'); ?></span>
                                </div>
                            </div>
                        </div>    
                    <?php 
                        endwhile;
                    endif;

                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
    </div>    
</main>

<?php
get_footer();