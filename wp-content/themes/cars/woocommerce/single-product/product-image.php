<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters(
	'woocommerce_single_product_image_gallery_classes',
	array(
		'woocommerce-product-gallery',
		'woocommerce-product-gallery--' . ( $product->get_image_id() ? 'with-images' : 'without-images' ),
		'woocommerce-product-gallery--columns-' . absint( $columns ),
		'images',
	)
);
?>
<div>
    <div id="main_area">
        <!-- Slider -->
        <div class="row">
            <div class="col-xs-12" id="slider">
                <!-- Top part of the slider -->
                <div class="row">
                    <div class="col-sm-12" id="carousel-bounding-box">
                        <div class="carousel slide" id="myCarousel">
                            <!-- Carousel items -->
                            <div class="carousel-inner">

                                <?php
                                $c = 0;
                                $product = new WC_product(get_the_ID());
                                $attachment_ids = $product->get_gallery_image_ids();
                                foreach( $attachment_ids as $attachment_id ){
                                    $c++;
                                    $class = ($c == 1) ? 'active' : '';
                                    ?>


                                <div class="item <?php echo $class; ?>" data-slide-number="<?php echo wp_get_attachment_url( $attachment_id ); ?>">
                                    <img src="<?php echo wp_get_attachment_url( $attachment_id ); ?>" alt="<?php the_title(); ?>"></div>
                                <?php } ?>


                            </div>

                            <!-- Carousel nav -->
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"><i class="fa fa-caret-left"></i></span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"><i class="fa fa-caret-right"></i></span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--/Slider-->

        <div class=" hidden-xs" id="slider-thumbs">
            <!-- Bottom switcher of slider -->
            <ul class="hide-bullets row">
                <?php
                $c = 0;
                $product = new WC_product(get_the_ID());
                $attachment_ids = $product->get_gallery_image_ids();
                foreach( $attachment_ids as $attachment_id ){

                $class = ($c == 1) ? 'active' : '';
                ?>
                <li class="col-sm-2 no-padding">
                    <a class="thumbnail" id="carousel-selector-<?php echo $c;?>"> <img src="<?php echo wp_get_attachment_url( $attachment_id,'small' ); ?>" alt="<?php the_title(); ?>"></a>
                </li>
                <?php  $c++; } ?>

            </ul>
        </div>
    </div>
</div>
<!--<div class="--><?php //echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?><!--" data-columns="--><?php //echo esc_attr( $columns ); ?><!--" style="opacity: 0; transition: opacity .25s ease-in-out;">-->
<!--	<figure class="woocommerce-product-gallery__wrapper">-->
<!--		--><?php
//		if ( $product->get_image_id() ) {
//			$html = wc_get_gallery_image_html( $post_thumbnail_id, true );
//		} else {
//			$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
//			$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src( 'woocommerce_single' ) ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
//			$html .= '</div>';
//		}
//
//		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id ); // phpcs:disable WordPress.XSS.EscapeOutput.OutputNotEscaped
//
//		do_action( 'woocommerce_product_thumbnails' );
//		?>
<!--	</figure>-->
<!--</div>-->
