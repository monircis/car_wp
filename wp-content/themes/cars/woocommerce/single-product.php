<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">

                <?php
                /**
                 * woocommerce_before_main_content hook.
                 *
                 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                 * @hooked woocommerce_breadcrumb - 20
                 */
                do_action('woocommerce_before_main_content');
                ?>
                <div class="deatil-info">
                    <?php while (have_posts()) : ?>
                        <?php the_post(); ?>


                        <?php wc_get_template_part('content', 'single-product'); ?>


                    <?php endwhile; // end of the loop. ?>

                    <?php
                    /**
                     * woocommerce_after_main_content hook.
                     *
                     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                     */
                    //do_action('woocommerce_after_main_content');
                    ?>

                    <?php
                    /**
                     * woocommerce_sidebar hook.
                     *
                     * @hooked woocommerce_get_sidebar - 10
                     */
                    //do_action('woocommerce_sidebar');
                    ?>
                </div>
            </div>
            <div class="col-md-7">
                <div class="deatil_info">
                    <div>
                        <?php while (have_posts()) : ?>
                            <h1 class="cartitle"><?php the_title(); ?></h1>
                            <p class="subinfo">  <?php
                                $terms = get_the_terms($post->ID, 'product_cat');
                                if ($terms && !is_wp_error($terms)) :
                                    if (!empty($terms)) {
                                        echo $terms[0]->name;
                                    } ?>
                                <?php endif; ?>
                                &nbsp;
                                <?php

                                if (get_the_terms($post->ID, 'country')) {
                                    echo '<i class="fa fa-map-marker red"></i> ';
                                    foreach (get_the_terms($post->ID, 'country') as $albumGenre) {
                                        echo $albumGenre->name; // or whatever value
                                    }
                                }
                                ?> &nbsp;<i class="fa fa-clock-o red ml15"
                                            aria-hidden="true"></i> <?php echo get_the_date(); ?></p>
                            <?php the_post(); ?>

                        <?php endwhile; // end of the loop. ?>

                        <?php
                        /**
                         * woocommerce_after_main_content hook.
                         *
                         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                         */
                        do_action('woocommerce_after_main_content');
                        ?>

                        <?php
                        /**
                         * woocommerce_sidebar hook.
                         *
                         * @hooked woocommerce_get_sidebar - 10
                         */
                        do_action('woocommerce_sidebar');
                        ?>
                        <span class="black <?php echo esc_attr(apply_filters('woocommerce_product_price_class', 'price')); ?>">
                    <?php echo $product->get_price_html(); ?>
</span>
                    </div>
                    <div class="listing-btn btnlists">
                        <?php


                        if ( ! $product->is_purchasable() ) {
                            return;
                        }

                        echo wc_get_stock_html( $product ); // WPCS: XSS ok.

                        if ( $product->is_in_stock() ) : ?>

                            <?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

                            <form class="cartform" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
                                <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

                                <?php
                                do_action( 'woocommerce_before_add_to_cart_quantity' );

                                woocommerce_quantity_input(
                                    array(
                                        'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                                        'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                                        'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
                                    )
                                );

                                do_action( 'woocommerce_after_add_to_cart_quantity' );
                                ?>

                                <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

                                <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
                            </form>

                            <?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

                        <?php endif; ?>


                        <a href="" class="listing-header-btn listing-btn-style2 morebtn">&nbsp; Know More About This Car &nbsp;</a>
                    </div>
                    <div class="table-responsive" style="margin-top: 30px">
                        <table class="table-bordered tabledetail">
                            <?php do_action('woocommerce_product_meta_start'); ?>
                            <!--                      <tr>-->
                            <!--                          -->
                            <!---->
                            <!---->
                            <!--                          <td colspan="2" class="metacolor">-->
                            <!---->
                            <!---->
                            <!--                              --><?php //echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
                            <!---->
                            <!--                              --><?php //echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
                            <!--                          </td>-->
                            <!---->
                            <!---->
                            <!--                          --><?php //do_action( 'woocommerce_product_meta_end' ); ?>
                            <!--                      </tr>-->

                            <tr>
                                <td><b>SKU:</b></td>
                                <td>
                                    <?php if (wc_product_sku_enabled() && ($product->get_sku() || $product->is_type('variable'))) : ?>

                                        <span class="sku_wrapper"><?php esc_html_e('', 'woocommerce'); ?> <span
                                                    class="sku"><?php echo ($sku = $product->get_sku()) ? $sku : esc_html__('N/A', 'woocommerce'); ?></span></span>

                                    <?php endif; ?>
                                </td>

                            </tr>
                            <tr>
                                <td><b>Country:</b></td>
                                <td><span class="black">	  <?php

                                        if (get_the_terms($post->ID, 'country')) {
                                            foreach (get_the_terms($post->ID, 'country') as $albumGenre) {
                                                echo $albumGenre->name; // or whatever value
                                            }
                                        }
                                        ?></span></td>
                            </tr>
                            <tr>
                                <td><b>Miliage:</b></td>
                                <td><span class="black"><?php the_field('miliage'); ?> KM</span></td>


                            </tr>
                            <tr>
                                <td><b>Body Type:</b></td>
                                <td><span class="black"><?php the_field('body_type'); ?></span></td>
                            </tr>

                            <tr>
                                <td><b>Seat:</b></td>
                                <td><span class="black"><?php the_field('Seat'); ?></span></td>
                            </tr>
                            <tr>
                                <td><b>Non Smoking Car:</b></td>
                                <td><span class="black"><?php the_field('non_smoking__car'); ?></span></td>


                            </tr>

                            <tr>
                                <td><b>Engine type:</b></td>
                                <td><span class="black"><?php the_field('engine_type'); ?></span></td>
                            </tr>
                            <tr>

                                <td><b>Drive system:</b></td>
                                <td><span class="black"><?php the_field('drive_system'); ?></span></td>

                            </tr>
                            <tr>
                                <td><b>Warranty:</b></td>
                                <td><span class="black"><?php the_field('warranty'); ?></span></td>
                                <td><b>Color:</b></td>
                                <td><span class="black"><?php the_field('color'); ?></span></td>

                            </tr>
                            <tr>
                                <td><b>Riding capacity:</b></td>
                                <td><span class="black"><?php the_field('riding_capacity'); ?></span></td>
                                <td><b>Number of doors:</b></td>
                                <td><span class="black"><?php the_field('number_of_doors'); ?></span></td>

                            </tr>
                            <tr>
                                <td><b>Mission:</b></td>
                                <td><span class="black"><?php the_field('mission'); ?></span></td>
                                <td><b>Chassis End Number:</b></td>
                                <td><span class="black"><?php the_field('chassis_end_number'); ?></span></td>

                            </tr>
                            <tr>
                                <td><b>Displacement:</b></td>
                                <td><span class="black"><?php the_field('displacement'); ?></span></td>
                                <td><b>Handle:</b></td>
                                <td><span class="black"><?php the_field('handle'); ?></span></td>

                            </tr>
                            <tr>
                                <td><b>Area:</b></td>
                                <td><span class="black"><?php the_field('area'); ?></span></td>
                                <td><b>Unused Vehicle:</b></td>
                                <td><span class="black"><?php the_field('unused_vehicle'); ?></span></td>

                            </tr>

                            <tr>
                                <td class="black" colspan="4">
                                    <div style="margin-top: 30px; font-weight: 500; margin-bottom: -10px;">                                   Description:
                                    </div>

                                    <?php the_content(); ?> </td>

                            </tr>

                        </table>
                    </div>
                </div>

            </div>
<!--            <div class="col-md-3">-->
<!---->
<!--                <div class="buttonarea">-->
<!--                    <h3>Want  to  buy this  product ?</h3>-->
<!---->
<?php
//
//?>
<!---->
<!--                    <div class="listing-btn">-->
<!--                        <a href="" class="listing-header-btn listing-btn-style2" style="margin-top: 15px;background: #3eb549 !important">Add  product to Cart</a>-->
<!---->
<!--                   <a href="" class="listing-header-btn listing-btn-style2" style="margin-top: 19px">Know More About This Car</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--              --><?php
//                $vendor_id = get_the_author_meta('ID');
////                // Get the WP_User object (the vendor) from author ID
//                $vendor = new WP_User($vendor_id);
//                $current_user = wp_get_current_user();
//
//
//                ?>
<!--           <div class="verndorbox">-->
<!---->
<?php //echo get_avatar($vendor_id, 100); ?>
<!---->
<!--                  <h3>--><?php
//                        echo $vendor->first_name . ' ' . $vendor->last_name;
//                        ?><!--</h3>-->
<!--                    <div>Member Since:-->
<!--                        --><?php
//
//                        $user_data = get_userdata($user->ID);
//                        $registered_date = $user_data->user_registered;
//                        echo date("M Y", strtotime($registered_date));
//                        ?>
<!--                    </div>-->
<!--                    <div><a href="--><?php //echo $store_url = dokan_get_store_url($vendor_id);; ?><!--">Vendor All Posts</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                --><?php ////echo $vendor_id = get_the_author_meta( 'ID' ); ?>
<!--                --><?php
//                //			// Get the author ID (the vendor ID)
//                //			//$vendor_id = get_post_field( 'post_author', $vendor_id );
//                //			// Get the WP_User object (the vendor) from author ID
//                //			$vendor = new WP_User($vendor_id);
//                //
//                //            echo	$store_info  = dokan_get_store_info( $vendor_id ); // Get the store data
//                //            echo	$store_name  = $store_info['store_name'];          // Get the store name
//                //            echo	$store_url   = dokan_get_store_url( $vendor_id );  // Get the store URL
//                //			echo $vendor_name = $vendor->first_name;
//                //            echo $vendor_name = $vendor->display_name;              // Get the vendor name
//                //            echo	$address     = $vendor->billing_address_1;           // Get the vendor address
//                //            echo	$postcode    = $vendor->billing_postcode;          // Get the vendor postcode
//                //            echo	$city        = $vendor->billing_city;              // Get the vendor city
//                //            echo	$state       = $vendor->billing_state;             // Get the vendor state
//                //            echo	$country     = $vendor->billing_country;           // Get the vendor country
//
//                // Display the seller name linked to the store
//                //printf( '<b>Seller Name:</b> <a href="%s">%s</a>', $store_url, $store_name );
//                ?>
<!---->
<!--                --><?php
//                $terms = get_terms(array(
//                    'taxonomy' => 'country',
//                    'hide_empty' => false,
//                )); ?>
<!--                <div class="locationslist">-->
<!--                    <h3>Car Locations</h3>-->
<!--                       --><?php //// foreach ($terms as $term): ?>
<!--                -->
<!--                                   <div class="location">-->
<!--                                       <a href="-->
<!--                --><?php ////echo get_term_link($term->slug,'country'); ?><!--" title="USA"><span class="location-icon"><i class="pacz-icon-map-marker"></i></span>-->
<!--                --><?php ////echo $term->name; ?><!--<span-->
<!--                <                                 class="location-count"></span></a>-->
<!--                                   </div>-->
               <!---->
 <?php ////endforeach; ?>
            <!--                 </div>-->
            <!--    </div>-->
            <div class="bottombox">
               <div class="col-md-12">
                   <h2>Related Cars:</h2>
               </div>
                <?php
                global $post;
                $related = get_posts(
                    array(
                        'category__in' => wp_get_post_categories( $post->ID ),
                        'numberposts'  => 6,
                        'post__not_in' => array($post->ID),
                        'post_type'    => 'product'
                    )
                );
                if( $related ) { ?>
                    <div class="clearfix" style="margin-bottom: 30px">
                        <?php
                        foreach( $related as $post ) {
                            setup_postdata($post);
                            $url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'full' );
                            $product = wc_get_product( get_the_ID() );?>
                            <div class="col-md-3  ">
                                <div class="each-ecar">
                                    <div class="carimg">
                                        <a href="<?php echo get_the_permalink(get_the_ID()); ?>">
												<span class="cprice">
					   <span class="woocommerce-Price-amount amount"><bdi>
                               <span class="woocommerce-Price-currencySymbol">৳&nbsp;</span><?php echo $product->get_price(); ?></bdi></span> </span>
                                            <img src="<?php echo $url; ?>">
                                        </a>
                                    </div>
                                    <div class="carsummary">
                                        <p class="brand">
                                            <?php
                                            $terms = get_the_terms(get_the_ID(), 'product_cat');
                                            if ($terms && !is_wp_error($terms)) :
                                                if (!empty($terms)) {
                                                    echo $terms[0]->name;
                                                } ?>
                                            <?php endif; ?>                                                    </p>
                                        <h4>
                                            <a href="<?php echo get_the_permalink(get_the_ID()); ?>"><?php echo get_the_title(); ?></a>
                                        </h4>
                                        <div class="country">
                                            <i class="fa fa-map-marker">
                                            </i>
                                            <?php
                                            if(get_the_terms( get_the_ID(), 'country' )){
                                                foreach ( get_the_terms( $post->ID, 'country' ) as $albumGenre ) {
                                                    echo $albumGenre->name; // or whatever value
                                                }
                                            }
                                            ?>


                                                                     </div>
                                        <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                            <div class="btn-group" role="group">
                                                <div class="spec">
                                                    <?php the_field('miliage'); ?> KM
                                                </div>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <div class="spec">
                                                    <?php the_field('car_type'); ?>                               </div>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <div class="spec">
                                                    <?php the_field('Seat'); ?>                             </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
                            /*whatever you want to output*/
                        }
                        wp_reset_postdata(); ?>
                    </div>
                <?php } ?>

                <?php
                $product_tabs = apply_filters( 'woocommerce_product_tabs', array() );

                if ( ! empty( $product_tabs ) ) : ?>

               <div class="col-md-12">
                   <div class="woocommerce-tabs wc-tabs-wrapper">
                       <ul class="tabs wc-tabs" role="tablist">
                           <?php foreach ( $product_tabs as $key => $product_tab ) : ?>
                               <li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-<?php echo esc_attr( $key ); ?>" role="tab" aria-controls="tab-<?php echo esc_attr( $key ); ?>">
                                   <a href="#tab-<?php echo esc_attr( $key ); ?>">
                                       <?php echo wp_kses_post( apply_filters( 'woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key ) ); ?>
                                   </a>
                               </li>
                           <?php endforeach; ?>
                       </ul>
                       <?php foreach ( $product_tabs as $key => $product_tab ) : ?>
                           <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content wc-tab" id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel" aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
                               <?php
                               if ( isset( $product_tab['callback'] ) ) {
                                   call_user_func( $product_tab['callback'], $key, $product_tab );
                               }
                               ?>
                           </div>
                       <?php endforeach; ?>

                       <?php do_action( 'woocommerce_product_after_tabs' ); ?>
                   </div>
               </div>

                <?php endif; ?>


            </div>
        </div>
    </div>

<?php
get_footer('shop');

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
