<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
?>
<div class="container-fluid">
<?php
/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
?>
    <div class="row">
        <div class="col-md-12  ">
<?php
do_action( 'woocommerce_before_main_content' );
?>
<div class="adv-filter">
    <div class="row">
        <div class="col-md-4 ">
            <input type="text" class="form-control"/>
        </div>
        <div class="col-md-2 pdlr7">
            <select name="action" class="js-example-basic-single js-states form-control" required>
                <option value="toPrepare">
                    Prepare
                </option>
            </select>
        </div>
        <div class="col-md-2 pdlr7">
            <select name="action" class="js-example-basic-single js-states form-control" required>
                <option value="toPrepare">
                    Make
                </option>
            </select>
        </div>
        <div class="col-md-2 pdlr7">
            <select name="action" class="js-example-basic-single js-states form-control" required>
                <option value="toPrepare">
                    Model
                </option>
            </select>
        </div>
        <div class="col-md-2 ">
            <button class="btn btn-sm btn-base btn-block">
                Search
            </button>
        </div>

    </div>

</div>

<!--      <header class="woocommerce-products-header">-->
<!--          --><?php //if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
<!--              <h1 class="woocommerce-products-header__title page-title">--><?php //woocommerce_page_title(); ?><!--</h1>-->
<!--          --><?php //endif; ?>
<!---->
<!--          --><?php
//          /**
//           * Hook: woocommerce_archive_description.
//           *
//           * @hooked woocommerce_taxonomy_archive_description - 10
//           * @hooked woocommerce_product_archive_description - 10
//           */
//          do_action( 'woocommerce_archive_description' );
//          ?>
<!--      </header>-->
  </div>

</div>

<?php
if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );?>
            <div class="col-md-3  ">
                <div class="each-ecar">
                    <div class="carimg">
                        <a href="<?php the_permalink(); ?>">
												<span class="cprice">
													<?php $price = get_post_meta(get_the_ID(), '_price', true); ?>
                                                    <?php echo wc_price($price); ?>
 </span>
                            <?php the_post_thumbnail(array(370, 260)); ?>
                            <!--<img class="img-circle imguser" width="40px" height="40px" src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/bfi_thumb/img-02-2-ohe3ctp1r0c0a6pjv6r2h72me1tj94f39ag9u943gw.jpg" />
                            -->
                        </a>
                    </div>
                    <div class="carsummary">
                        <p class="brand">
                            <?php
                            $terms = get_the_terms($post->ID, 'product_cat');
                            if ($terms && !is_wp_error($terms)) :
                                if (!empty($terms)) {
                                    echo $terms[0]->name;
                                } ?>
                            <?php endif; ?>
                        </p>
                        <h4>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </h4>
                        <div class="country">
                            <i class="fa fa-map-marker">
                            </i>
                            <?php

                            $albumGenres = get_the_terms( $post->ID, 'country' );
                            foreach ( $albumGenres as $albumGenre ) {
                                echo $albumGenre->name; // or whatever value
                            }

                            ?>
                        </div>
                        <div class="btn-group btn-group-justified" role="group" aria-label="...">
                            <div class="btn-group" role="group">
                                <div class="spec">
                                    <?php the_field('miliage'); ?> KM
                                </div>
                            </div>
                            <div class="btn-group" role="group">
                                <div class="spec">
                                    <?php the_field('car_type'); ?>
                                </div>
                            </div>
                            <div class="btn-group" role="group">
                                <div class="spec">
                                    <?php the_field('Seat'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php

			//wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}
?>
</div>
<?php
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
