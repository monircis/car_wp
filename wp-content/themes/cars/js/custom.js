

jQuery('.js-example-basic-single').select2({});


    var swiperH = new Swiper('.swiper-container-h', {
      spaceBetween: 50,
      pagination: {
        el: '.swiper-pagination-h',
        clickable: true,
      },
    });
    var swiperV = new Swiper('.swiper-container-v', {
      direction: 'vertical',
      spaceBetween: 50,
      pagination: {
        el: '.swiper-pagination-v',
        clickable: true,
      },
    });


    var swiper = new Swiper('.slider_full', {
      slidesPerView: 4,
      spaceBetween: 10,
      // init: false,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      breakpoints: {
        1024: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 10,
        },
        320: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }
    });




 jQuery(document).ready(function($) {
   //set here the speed to change the slides in the carousel
   $('#myCarousel').carousel({
     interval: 5000
   });

//Loads the html to each slider. Write in the "div id="slide-content-x" what you want to show in each slide
   $('#carousel-text').html($('#slide-content-0').html());

   //Handles the carousel thumbnails
   $('[id^=carousel-selector-]').click( function(){
     var id = this.id.substr(this.id.lastIndexOf("-") + 1);
     var id = parseInt(id);
     $('#myCarousel').carousel(id);
   });


   // When the carousel slides, auto update the text
   $('#myCarousel').on('slid.bs.carousel', function (e) {
     var id = $('.item.active').data('slide-number');
     $('#carousel-text').html($('#slide-content-'+id).html());
   });
 });