<?php
/*
 Template Name: Home Page
 */
get_header();
?>
    <div class="banner-area">
        <div class="each"
             style="padding-bottom: 50px;background: url('https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/2019/03/bbg.jpg?id=3241');">
            <div class="container-fluid">
                <h1 class="banner-title">
                    Find Cars
                    <br/>
                    Special for you
                </h1>
                <p class="subtitle">
                    With thousands of cars,
                    <br/>
                    we have just the right one for you
                </p>
            </div>
        </div>
    </div>
    <div class="advance-search">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 pdlr7 rel">
                    <div class="btn-group type" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-default">
                            New Cars
                        </button>
                        <button type="button" class="btn btn-default active">
                            Used Cars
                        </button>
                    </div>
                </div>
                <div class="col-md-4 pdlr7">
                    <input type="text" class="form-control"/>
                </div>
                <div class="col-md-2 pdlr7">
                    <select name="action" class="js-example-basic-single js-states form-control" required>
                        <option value="toPrepare">
                            Prepare
                        </option>
                    </select>
                </div>
                <div class="col-md-2 pdlr7">
                    <select name="action" class="js-example-basic-single js-states form-control" required>
                        <option value="toPrepare">
                            Make
                        </option>
                    </select>
                </div>
                <div class="col-md-2 pdlr7">
                    <select name="action" class="js-example-basic-single js-states form-control" required>
                        <option value="toPrepare">
                            Model
                        </option>
                    </select>
                </div>
                <div class="col-md-2 pdlr7">
                    <button class="btn btn-sm btn-base btn-block">
                        Search
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="carmodel">
        <div class="container-fluid">
            <div class="row">
                <?php

                $orderby = 'name';
                $order = 'asc';
                $hide_empty = false;
                $cat_args = array(
                    'orderby' => $orderby,
                    'order' => $order,
                    'hide_empty' => $hide_empty,
                );

                $product_categories = get_terms('product_cat', $cat_args);

                foreach ($product_categories as $key => $category) {
                    $thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true); ?>

                    <div class="col-md-2 pdlr7 mb5">
                        <a href="<?php echo get_term_link($category) ?>">
                            <div class="eachmodel">
                                <?php $image = wp_get_attachment_image_src($thumbnail_id, 'woo-size-category');
                                echo '<img src="' . esc_url($image[0]) . '" alt="' . esc_attr($category->name) . '" height="100px" />';
                                ?>
                                <p><?php echo $category->name ?></p>
                            </div>
                        </a>
                    </div>
                <?php }
                ?>


            </div>
        </div>
    </div>
    <div class="slider_lastes">
        <div class="container-fluid">

            <div class="swiper-container slider_full">
                <div class="swiper-wrapper">

                    <?php


                    $args = array(
                        'post_type' => 'product',
                        'meta_key' => 'is_featured', //Your Custom field name
                        'meta_value' => 'Yes', //Custom field value
                    );


                    // query
                    $the_query = new WP_Query($args);
                    if ($the_query->have_posts()): ?>

                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <div class="swiper-slide">
                                <div class="each-ecar">
                                    <div class="carimg">
                                        <a href="<?php the_permalink(); ?>">
												<span class="cprice">
													<?php $price = get_post_meta(get_the_ID(), '_price', true); ?>
                                                    <?php echo wc_price($price); ?>
 </span>
                                            <?php the_post_thumbnail(array(370, 260)); ?>
                                            <!--<img class="img-circle imguser" width="40px" height="40px" src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/bfi_thumb/img-02-2-ohe3ctp1r0c0a6pjv6r2h72me1tj94f39ag9u943gw.jpg" />
                                            -->
                                        </a>
                                    </div>
                                    <div class="carsummary">
                                        <p class="brand">
                                            <?php
                                            $terms = get_the_terms($post->ID, 'product_cat');
                                            if ($terms && !is_wp_error($terms)) :
                                                if (!empty($terms)) {
                                                    echo $terms[0]->name;
                                                } ?>
                                            <?php endif; ?>
                                        </p>
                                        <h4>
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </h4>
                                        <div class="country">
                                            <i class="fa fa-map-marker">
                                            </i>

                                            <?php

                                            $albumGenres = get_the_terms( $post->ID, 'country' );
                                            foreach ( $albumGenres as $albumGenre ) {
                                                echo $albumGenre->name; // or whatever value
                                            }

                                            ?>
                                        </div>
                                        <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                            <div class="btn-group" role="group">
                                                <div class="spec">
                                                    <?php the_field('miliage'); ?> KM
                                                </div>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <div class="spec">
                                                    <?php the_field('car_type'); ?>
                                                </div>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <div class="spec">
                                                    <?php the_field('Seat'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>

                    <?php endif; ?>

                    <?php wp_reset_query();     // Restore global post data stomped by the_post(). ?>


                </div>

            </div>
        </div>
    </div>
    <div class="latest">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3>
                        Latest Motors
                    </h3>
                    <p class="mb30">
                        Browse you desire one from the listed below.
                    </p>
                </div>


                <?php
                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 12
                );
                $loop = new WP_Query($args);
                if ($loop->have_posts()) {
                    while ($loop->have_posts()) : $loop->the_post(); ?>

                        <div class="col-md-3 pdlr7">
                            <div class="each-ecar">
                                <div class="carimg">
                                    <a href="<?php the_permalink(); ?>">
												<span class="cprice">
													<?php $price = get_post_meta(get_the_ID(), '_price', true); ?>
                                                    <?php echo wc_price($price); ?>
 </span>
                                        <?php the_post_thumbnail(array(370, 260)); ?>
                                        <!--<img class="img-circle imguser" width="40px" height="40px" src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/bfi_thumb/img-02-2-ohe3ctp1r0c0a6pjv6r2h72me1tj94f39ag9u943gw.jpg" />
                                        -->
                                    </a>
                                </div>
                                <div class="carsummary">
                                    <p class="brand">
                                        <?php
                                        $terms = get_the_terms($post->ID, 'product_cat');
                                        if ($terms && !is_wp_error($terms)) :
                                            if (!empty($terms)) {
                                                echo $terms[0]->name;
                                            } ?>
                                        <?php endif; ?>
                                    </p>
                                    <h4>
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h4>
                                    <div class="country">
                                        <i class="fa fa-map-marker">
                                        </i>
                                        <?php
                                        if(get_the_terms( $post->ID, 'country' )){
                                            foreach ( get_the_terms( $post->ID, 'country' ) as $albumGenre ) {
                                                echo $albumGenre->name; // or whatever value
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <div class="btn-group" role="group">
                                            <div class="spec">
                                                <?php the_field('miliage'); ?> KM
                                            </div>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <div class="spec">
                                                <?php the_field('car_type'); ?>
                                            </div>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <div class="spec">
                                                <?php the_field('Seat'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    <?php //wc_get_template_part( 'content', 'product' );
                    endwhile;
                } else {
                    echo __('No products found');
                }
                wp_reset_postdata();
                ?>

            </div>
        </div>
    </div>
    <div class="latest ourlocations">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3>
                        Motors Locations
                    </h3>
                    <p>
                        Select your location to find your dream car.
                    </p>
                </div>
                <div class="locations">
                    <div class="row">

                        <?php
                        $terms = get_terms( array(
                            'taxonomy' => 'country',
                            'hide_empty' => false,
                        ) );

                        foreach ($terms as $term): ?>
                            <div class="col-md-3">
                                <div class="location">
                                    <a href="<?php echo get_term_link($term->slug,'country'); ?>" title="USA"><span class="location-icon"><i class="pacz-icon-map-marker"></i></span><?php echo $term->name; ?><span
                                            class="location-count"></span></a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonial">
        <div class="swiper-container swiper-container-h">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/bfi_thumb/testi-author1-ohe3ctp2hblv8zlsij9rqudn0gcig6cisd7dj3efhk.png"/>
                    <p>Efficiently unleash cross-media information without crossmedia value. Quickly <br/> maximize
                        timely deliverables for realtime schemas Dramatically maintain.</p>
                    <div class="pdesignation">
                        Monirul Islam<br/>
                        <small>Software Engineer</small>
                    </div>
                </div>
                <div class="swiper-slide">
                    <img src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/bfi_thumb/testi-author1-ohe3ctp2hblv8zlsij9rqudn0gcig6cisd7dj3efhk.png"/>
                    <p>Efficiently unleash cross-media information without crossmedia value. Quickly <br/> maximize
                        timely deliverables for realtime schemas Dramatically maintain.</p>
                    <div class="pdesignation">
                        Monirul Islam<br/>
                        <small>Software Engineer</small>
                    </div>
                </div>
                <div class="swiper-slide">
                    <img src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/bfi_thumb/testi-author1-ohe3ctp2hblv8zlsij9rqudn0gcig6cisd7dj3efhk.png"/>
                    <p>Efficiently unleash cross-media information without crossmedia value. Quickly <br/> maximize
                        timely deliverables for realtime schemas Dramatically maintain.</p>
                    <div class="pdesignation">
                        Monirul Islam<br/>
                        <small>Software Engineer</small>
                    </div>
                </div>
                <div class="swiper-slide">
                    <img src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/bfi_thumb/testi-author1-ohe3ctp2hblv8zlsij9rqudn0gcig6cisd7dj3efhk.png"/>
                    <p>Efficiently unleash cross-media information without crossmedia value. Quickly <br/> maximize
                        timely deliverables for realtime schemas Dramatically maintain.</p>
                    <div class="pdesignation">
                        Monirul Islam<br/>
                        <small>Software Engineer</small>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="features-list">
        <div class="container-fluid">

            <div class="row">

                <?php
                $args = array('post_type' => 'features', 'posts_per_page' => 10);
                $the_query = new WP_Query($args);
                ?>

                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                    <div class="col-md-3">
                        <div class="fbox">
                            <div class="roundbox">
                                <?php the_post_thumbnail(array(100, 100)); ?>
                            </div>
                            <h4><?php the_title(); ?></h4>
                            <p><?php the_content(); ?></p>
                        </div>
                    </div>

                <?php endwhile; ?>

                <div class="clearfix"></div>
                <div class="col-md-6">
                    <img class="img-responsive"
                         src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/2019/05/img.png" alt=""/>
                </div>
                <div class="col-md-6 app-info">
                    <h3>Get Motoro App for your mobile</h3>
                    <p>Qirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum
                        formas humanitatis per seacula quarta decima et quinta decima.</p>
                    <img src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/2019/03/play-store-download.png"
                         alt=""/>
                </div>

            </div>
        </div>

    </div>
    <div class="highligts">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2>Get to start the Motoro right now!<br/>
                        <small>Sed diam nonummy nibh euismod tincidunt.</small>
                    </h2>
                    <a class="btn btn-default pull-right">SIGN UP</a>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
    </div>
    </div>

<?php
get_footer();
?>