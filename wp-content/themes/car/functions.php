<?php
//display title and  tag  in  header

add_theme_support("title-tag");

//we_enqueue scripts

function ecmom_scripts(){
    //for style
    wp_enqueue_style('bootstrap',get_theme_file_uri('styles/css/bootstrap.min.css'),array(),'');
    wp_enqueue_style('slick',get_theme_file_uri('styles/css/slick/slick.css'),array(),'');
    wp_enqueue_style('slick-theme',get_theme_file_uri('styles/css/slick/slick-theme.css'),array(),'');
    wp_enqueue_style('style1',get_theme_file_uri('styles/css/styles.css'),array(),'');
    wp_enqueue_style('pacz',get_theme_file_uri('css/pacz-styles.css'),array(),'');
    wp_enqueue_style('pacz-blog',get_theme_file_uri('styles/css/pacz-blog.css'),array(),'');
    
    wp_enqueue_style('common',get_theme_file_uri('styles/css/shortcode/common-shortcode.css'),array(),'');
    wp_enqueue_style('fonticon',get_theme_file_uri('styles/css/fonticon-custom.min.css'),array(),'');
    wp_enqueue_style('motoro',get_theme_file_uri('css/motoro-dynamic.css'),array(),'');
    wp_enqueue_style('frontend',get_theme_file_uri('css/frontend-custom.css'),array(),'');
    wp_enqueue_style('swiper',get_theme_file_uri('css/swiper.min.css'),array(),'');
    
    wp_enqueue_style('submitlisting',get_theme_file_uri('css/submitlisting-custom.css'),array(),'');
    wp_enqueue_style('woocommercedssd',get_theme_file_uri('styles/css/pacz-woocommercedssd.css'),array(),'');
    wp_enqueue_style('custom',get_theme_file_uri('css/custom.css'),array(),'');
       
       
    //for scripts  
    wp_enqueue_script('bootstrapjs',get_theme_file_uri('js/bootstrap.min.js'),array(),'',true);
    wp_enqueue_script('swiperjs',get_theme_file_uri('js/swiper.min.js'),array(),'',true);
      wp_enqueue_script('customjs',get_theme_file_uri('js/custom.js'),array(),'',true);
     
}
add_action('wp_enqueue_scripts','ecmom_scripts');

	     
/*
Register Menu Support
*/	
	 
function ecomMenu(){
    if(function_exists('register_nav_menu')){
     register_nav_menu('main_menu',__('Main Menu','Car'));    
     register_nav_menu('footer_menu',__('Footer Menu','Car')); 
    }
}

add_action('init','ecomMenu');


/*
Image Support
*/	
add_theme_support( 'post-thumbnails' );
//add_theme_support('post-thumbnails',array('post','page'));
//set_post_thumbnail_size(300,200,true);
//add_image_size('sample',1300,700,true);

function car_widgets(){
    register_sidebar(array(
    'name' =>esc_html__('Footer Widget First','Car'),
    'description'=>esc_html__('This is Left Footer Widget','Car'),
    'id'=>'footer_widget_one',
    'before_widget'=>'<div class="pacz-col-1-4"><section id="text-7" class="widget widget_text">',
     'after_widget'=>'</section></div>',
      'before_title'=>'<div class="widgettitle">',
      'after_title'=>'</div>'
    ));
    
   register_sidebar(array(
    'name' =>esc_html__('Footer Widget Second','Car'),
    'description'=>esc_html__('This is Middle Footer Widget','Car'),
    'id'=>'footer_widget_two',
    'before_widget'=>'<div class="pacz-col-1-4"><section id="text-7" class="widget widget_text">',
     'after_widget'=>'</section></div>',
      'before_title'=>'<div class="widgettitle">',
      'after_title'=>'</div>'
    ));
    register_sidebar(array(
    'name' =>esc_html__('Footer Widget Third','Car'),
    'description'=>esc_html__('This is Middle Footer Widget','Car'),
    'id'=>'footer_widget_three',
    'before_widget'=>'<div class="pacz-col-1-4"><section id="text-7" class="widget widget_text">',
     'after_widget'=>'</section></div>',
      'before_title'=>'<div class="widgettitle">',
      'after_title'=>'</div>'
    ));
    register_sidebar(array(
    'name' =>esc_html__('Footer Widget Fourth','Car'),
    'description'=>esc_html__('This is Right Footer Widget','Car'),
    'id'=>'footer_widget_four',
    'before_widget'=>'<div class="pacz-col-1-4"><section id="text-7" class="widget widget_text">',
     'after_widget'=>'</section></div>',
      'before_title'=>'<div class="widgettitle">',
      'after_title'=>'</div>'
    ));
}

    
add_action('widgets_init','car_widgets');


// Our custom post type function
function car_feature_post_type() {
$supports = array(
'title', // post title
'editor', // post content
'thumbnail', // featured images
//'author', // post author
//'excerpt', // post excerpt
//'custom-fields', // custom fields
//'comments', // post comments
//'revisions', // post revisions
//'post-formats', // post formats
);
$labels = array(
'name' => _x('features', 'plural'),
'singular_name' => _x('features', 'singular'),
'menu_name' => _x('Features', 'admin menu'),
'name_admin_bar' => _x('features', 'admin bar'),
'add_new' => _x('Add New', 'add new'),
'add_new_item' => __('Add New features'),
'new_item' => __('New features'),
'edit_item' => __('Edit features'),
'view_item' => __('View features'),
'all_items' => __('All features'),
'search_items' => __('Search features'),
'not_found' => __('No features found.'),
);
$args = array(
'supports' => $supports,
'labels' => $labels,
'public' => true,
'query_var' => true,
'rewrite' => array('slug' => 'features'),
'has_archive' => true,
'hierarchical' => false,
);
register_post_type('features', $args);
}
add_action('init', 'car_feature_post_type');
/*Custom Post type end*/


//woocommercedssd  theme  support

function car_woocommerce_theme_support(){
    add_theme_support('woocommercedssd');
}
add_action('after_setup_theme',"car_woocommerce_theme_support");

 


//taxonomy

/**
 * Register a 'genre' taxonomy for post type 'book'.
 *
 * @see register_post_type for registering post types.
 */
function wpdocs_create_book_tax() {
    register_taxonomy( 'country', 'product', array(
        'label'        => __( 'Country', 'textdomain' ),
        'rewrite'      => array( 'slug' => 'country' ),
        'hierarchical' => true,
    ) );
}
add_action( 'init', 'wpdocs_create_book_tax', 0 );





































