<?php
get_template_part("template-parts/header/header",'header');
?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


                <div class="singlepost">
                    <div class="each-ecar">
                        <h1><?php the_title(); ?></h1>
                        <p>  <i class="fa fa-map-marker">
                            </i>
                            <?php

                            $albumGenres = get_the_terms( $post->ID, 'country' );
                            foreach ( $albumGenres as $albumGenre ) {
                                echo $albumGenre->name; // or whatever value
                            }

                            ?> <i class="fa fa-clock"></i> <?php echo get_the_date(); ?></p>
                        <div class="carimg">
                            <a href="<?php the_permalink(); ?>">
												<span class="cprice">
													<?php $price = get_post_meta(get_the_ID(), '_price', true); ?>
                                                    <?php echo wc_price($price); ?>
 </span>
                                <?php the_post_thumbnail(array(370, 260)); ?>
                                <!--<img class="img-circle imguser" width="40px" height="40px" src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/bfi_thumb/img-02-2-ohe3ctp1r0c0a6pjv6r2h72me1tj94f39ag9u943gw.jpg" />
                                -->
                            </a>
                        </div>
                        <div class="carsummary">
                            <p class="brand">
                                <?php
                                $terms = get_the_terms($post->ID, 'product_cat');
                                if ($terms && !is_wp_error($terms)) :
                                    if (!empty($terms)) {
                                        echo $terms[0]->name;
                                    } ?>
                                <?php endif; ?>
                            </p>
                            <h4>
                                <a href="<?php the_permalink(); ?>"></a>
                            </h4>
                            <div class="country">
                                <i class="fa fa-map-marker">
                                </i>
                                <?php

                                $albumGenres = get_the_terms( $post->ID, 'country' );
                                foreach ( $albumGenres as $albumGenre ) {
                                    echo $albumGenre->name; // or whatever value
                                }

                                ?>
                            </div>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                    <div class="spec">
                                        <?php the_field('miliage'); ?> KM
                                    </div>
                                </div>
                                <div class="btn-group" role="group">
                                    <div class="spec">
                                        <?php the_field('car_type'); ?>
                                    </div>
                                </div>
                                <div class="btn-group" role="group">
                                    <div class="spec">
                                        <?php the_field('Seat'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            <?php endif;
        ?>


        <?php wp_reset_postdata(); ?>
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>
<?php
//get_footer();
get_template_part("template-parts/footer/footer",'footer');
?>