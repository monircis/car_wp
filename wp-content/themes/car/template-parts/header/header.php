<!DOCTYPE html>
<html <?php language_attributes(); ?>
        class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <?php //bloginfo( "name"); ?>
    <?php //bloginfo( "description"); ?>
    <script src="https://kit.fontawesome.com/7a1c11509a.js" crossorigin="anonymous">
    </script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;1,300&display=swap"
          rel="stylesheet">
    <link rel='dns-prefetch' href='http://maxcdn.bootstrapcdn.com/'/>
    <link rel='dns-prefetch' href='http://ajax.googleapis.com/'/>
    <link rel='dns-prefetch' href='http://s.w.org/'/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" rel="stylesheet"/>
    <link rel='stylesheet' id='fontawsome-css-css'
          href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' type='text/css'
          media='all'/>
    <link rel='stylesheet'
          href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.min.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' href='<?php echo get_stylesheet_uri(); ?>' type='text/css'/>
    <?php wp_head(); ?>
</head>
<body class="home page-template-default page   ">
<div class="theme-main-wrapper ">
    <div id="pacz-boxed-layout" class="pacz-full-enabled">
        <header id="pacz-header"
                class=" boxed-header header-style-v11 header-align-left header-structure-full 0 header_grid_margin  theme-main-header pacz-header-module"
                data-header-style="block" data-header-structure="full"
                data-transparent-skin="dark" data-height="78" data-sticky-height="48">
            <div class="pacz-header-mainnavbar">
                <div class="pacz-grid clearfix">
                    <nav id="pacz-main-navigation" class="clearfix">
                        <ul id="menu-main" class="main-navigation-ul clearfix">
                            <li class="responsive-nav-link">
                                <div class="pacz-burger-icon">
                                    <div class="burger-icon-1">
                                    </div>
                                    <div class="burger-icon-2">
                                    </div>
                                    <div class="burger-icon-3">
                                    </div>
                                </div>
                            </li>
                            <li class="pacz-header-logo">
                                <a href="<?php echo home_url(); ?>"
                                   title="Motoro - Best Auto Dealer And Classified WordPress Theme"><img
                                            alt="Motoro - Best Auto Dealer And Classified WordPress Theme"
                                            class="pacz-dark-logo"
                                            src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/2019/03/logo2.png"
                                            data-retina-src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/2019/03/logo2.png"/></a>
                            </li>
                            <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'main_menu',
                                    'container_class' => ''
                                )
                            );
                            ?>


                            <li class="logreg-header right">
                                <a class="pacz-login-2 clearfix" href="login/index.html">login</a>
                                <a class="pacz-register-2" href="register/index.html">Register</a>
                            </li>
                            <li class="listing-btn right">
                                <a class="listing-header-btn listing-btn-style2" href="submit-listing/index.html">Post
                                    Your Ad</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <div class="sticky-header-padding ">
        </div>
        <div id="theme-page">
            <div class="pacz-main-wrapper-holder">