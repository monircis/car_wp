<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<!-- /Added by HTTrack -->
	<head>
			<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<title>
		</title>
		<script src="https://kit.fontawesome.com/7a1c11509a.js" crossorigin="anonymous">
			
    
		</script>
        	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;1,300&display=swap" rel="stylesheet">
		<link rel='dns-prefetch' href='http://maxcdn.bootstrapcdn.com/' />
		<link rel='dns-prefetch' href='http://ajax.googleapis.com/' />
		<link rel='dns-prefetch' href='http://s.w.org/' />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" rel="stylesheet" />
		<link rel='stylesheet' id='bootstrap-css' href='<?php bloginfo('template_url'); ?>/styles/css/bootstrap.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='slick-css' href='<?php bloginfo('template_url'); ?>/styles/css/slick/slick.css' type='text/css' media='all' />
		<link rel='stylesheet' id='slick-theme-css' href='<?php bloginfo('template_url'); ?>/styles/css/slick/slick-theme.css' type='text/css' media='all' />
		<link rel='stylesheet' id='pacz-styles-default-css' href='<?php bloginfo('template_url'); ?>/styles/css/styles.css' type='text/css' media='all' />
		<link rel='stylesheet' id='pacz-styles-css' href='<?php bloginfo('template_url'); ?>/css/pacz-styles.css' type='text/css' media='all' />
		<link rel='stylesheet' id='pacz-blog-css' href='<?php bloginfo('template_url'); ?>/styles/css/pacz-blog.css' type='text/css' media='all' />
		<link rel='stylesheet' id='pacz-common-shortcode-css' href='<?php bloginfo('template_url'); ?>/styles/css/shortcode/common-shortcode.css' type='text/css' media='all' />
		<link rel='stylesheet' id='pacz-fonticon-custom-css' href='<?php bloginfo('template_url'); ?>/styles/css/fonticon-custom.min.css' type='text/css' media='all' />
	 
		<link rel='stylesheet' id='fontawsome-css-css' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' type='text/css' media='all' />
		 
	
		<link rel='stylesheet' id='apsl-frontend-css-css' href='<?php bloginfo('template_url'); ?>/css/motoro-dynamic.css' type='text/css' media='all' />
		<link rel='stylesheet' id='alsp_frontend-custom-css' href='<?php bloginfo('template_url'); ?>/css/frontend-custom.css' type='text/css' media='all' />
        <link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/css/swiper.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='alsp-jquery-ui-style-css' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.min.css' type='text/css' media='all' />
		<link rel='stylesheet' id='alsp_fsubmit-custom-css' href='<?php bloginfo('template_url'); ?>/css/submitlisting-custom.css' type='text/css' media='all' />
		<link rel='stylesheet' id='pacz-woocommerce-css' href='<?php bloginfo('template_url'); ?>/styles/css/pacz-woocommerce.css' type='text/css' media='all' />
       
		<link rel='stylesheet' id='apsl-frontend-css-css' href='<?php bloginfo('template_url'); ?>/css/custom.css' type='text/css' media='all' />
		<?php wp_head(); ?>
    
    </head>
	<body class="home page-template-default page   ">
		<div class="theme-main-wrapper ">
			<div id="pacz-boxed-layout" class="pacz-full-enabled">
				<header id="pacz-header" class=" boxed-header header-style-v11 header-align-left header-structure-full 0 header_grid_margin  theme-main-header pacz-header-module" data-header-style="block" data-header-structure="full"
				data-transparent-skin="dark" data-height="78" data-sticky-height="48">
					<div class="pacz-header-mainnavbar">
						<div class="pacz-grid clearfix">
							<nav id="pacz-main-navigation" class="clearfix">
								<ul id="menu-main" class="main-navigation-ul clearfix">
									<li class="responsive-nav-link">
										<div class="pacz-burger-icon">
											<div class="burger-icon-1">
											</div>
											<div class="burger-icon-2">
											</div>
											<div class="burger-icon-3">
											</div>
										</div>
									</li>
									<li class="pacz-header-logo">
										<a href="index.html" title="Motoro - Best Auto Dealer And Classified WordPress Theme"><img alt="Motoro - Best Auto Dealer And Classified WordPress Theme" class="pacz-dark-logo" src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/2019/03/logo2.png" data-retina-src="https://motoro.wpsixer.com/motoro-classo/wp-content/uploads/2019/03/logo2.png" /></a>
									</li>
									<li id="menu-item-1551" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-9 current_page_item no-mega-menu">
										<a class="menu-item-link" href="index.html">home</a>
									</li>
									<li id="menu-item-44" class="menu-item menu-item-type-post_type menu-item-object-page has-mega-menu">
										<a class="menu-item-link" href="new-motors/index.html">new motors</a>
									</li>
									<li id="menu-item-278" class="menu-item menu-item-type-post_type menu-item-object-page no-mega-menu">
										<a class="menu-item-link" href="blog/index.html">blog</a>
									</li>
									<li id="menu-item-613" class="menu-item menu-item-type-post_type menu-item-object-page no-mega-menu">
										<a class="menu-item-link" href="contact/index.html">contact</a>
									</li>
									<li class="logreg-header right">
										<a class="pacz-login-2 clearfix" href="login/index.html">login</a>
										<a class="pacz-register-2" href="register/index.html">Register</a>
									</li>
									<li class="listing-btn right">
										<a class="listing-header-btn listing-btn-style2" href="submit-listing/index.html">Post Your Ad</a>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</header>
				<div class="sticky-header-padding ">
				</div>
				<div id="theme-page">
					<div class="pacz-main-wrapper-holder">